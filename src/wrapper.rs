use std::env;
use std::process::Command;

fn main() {
    // Ignore first argument (this executable's name)
    let mut args = env::args_os().skip(1);
    let exec = args
        .next()
        .expect("portmod-wrapper received empty command!");
    let status = Command::new(&exec)
        .args(args)
        .status()
        .unwrap_or_else(|_| panic!("Wrapped command {} failed", exec.into_string().unwrap()));
    if let Some(code) = status.code() {
        std::process::exit(code);
    } else {
        std::process::exit(1);
    }
}
