# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""
A wrapper around logging that provides just the output functions
"""
from logging import (  # noqa  # pylint: disable=unused-import
    critical,
    debug,
    error,
    info,
    warning,
)
