#!/usr/bin/env python

# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3


import os
import sys
import json
import shutil
from subprocess import check_output
from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install
from setuptools_rust import Binding, RustExtension
from setuptools_rust.utils import Strip


# portmod_wrapper executable isn't installed with .exe extension
def fix_wrapper(self, debug: bool):
    metadata_command = [
        "cargo",
        "metadata",
        "--manifest-path",
        "Cargo.toml",
        "--format-version",
        "1",
    ]
    metadata = json.loads(check_output(metadata_command).decode("utf-8"))
    target_dir = metadata["target_directory"]
    if debug:
        suffix = "debug"
    else:
        suffix = "release"

    # location of cargo compiled files
    path = os.path.join(target_dir, suffix, "portmod_wrapper.exe")
    destpath = os.path.join(self.script_dir, "portmod_wrapper.exe")
    shutil.copy(path, destpath)


class PostDevelopCommand(develop):
    """Post-installation for development mode."""

    def run(self):
        develop.run(self)
        fix_wrapper(self, True)


class PostInstallCommand(install):
    """Post-installation for installation mode."""

    def run(self):
        install.run(self)
        fix_wrapper(self, False)


rust_extensions = [RustExtension("portmod.portmod", binding=Binding.PyO3, strip=True)]
cmdclass = {}
if sys.platform == "win32":
    # Wrapper is only needed for the Windows Sandbox
    rust_extensions.append(
        RustExtension(
            "portmod.portmod_wrapper",
            binding=Binding.Exec,
            strip=Strip.All,
            script=False,
        )
    )
    cmdclass = {"develop": PostDevelopCommand, "install": PostInstallCommand}


setup(
    name="portmod",
    author="Portmod Authors",
    author_email="incoming+portmod-portmod-9660349-issue-@incoming.gitlab.com",
    description="A CLI tool to manage mods for OpenMW",
    license="GPLv3",
    url="https://gitlab.com/portmod/portmod",
    download_url="https://gitlab.com/portmod/portmod/-/releases",
    packages=find_packages(exclude=["*.test", "*.test.*", "test.*", "test"]),
    rust_extensions=rust_extensions,
    cmdclass=cmdclass,
    zip_safe=False,
    entry_points=(
        {
            "console_scripts": [
                "inquisitor = portmod.inquisitor:main",
                "omwmerge = portmod.main:main",
                "omwmirror = portmod.mirror:mirror",
                "omwuse = portmod.omwuse:main",
                "openmw-conflicts = portmod.openmw_conflicts:main",
                "pybuild = portmod.omwpybuild:main",
                "omwselect = portmod.select:main",
                "omwquery = portmod.query:query_main",
            ]
        }
    ),
    install_requires=[
        "patool",
        "colorama",
        "appdirs",
        "black",
        "GitPython",
        "progressbar2",
        'pywin32; platform_system == "Windows"',
        "RestrictedPython>=4.0",
        "redbaron",
        'python-sat; platform_system != "Windows"',
        'python-sat>=0.1.5.dev12; platform_system == "Windows"',
        "requests",
        "chardet",
    ],
    setup_requires=["setuptools_scm", 'wheel; platform_system == "Windows"'],
    use_scm_version=True,
    extras_require={"test": ["pytest", "pytest-cov", "setuptools_scm"]},
)
